package com.miniproject.bookmanagementsystem.repository;

import com.miniproject.bookmanagementsystem.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;


@Repository
@CrossOrigin(origins = "http://localhost:4200")
public interface BookRepository extends JpaRepository<Book,Integer> {
    @RestResource(path = "bookTitle",rel = "search")
    Page<Book> findAllByTitleContainsIgnoreCase(@Param("title") String title, Pageable pageable);

    @RestResource(path = "bookCategory",rel = "searchCategory")
    Page<Book> findAllByCategoryTitle(@Param("title") String title, Pageable pageable);
}
