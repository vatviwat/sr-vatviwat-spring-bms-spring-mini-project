package com.miniproject.bookmanagementsystem.repository;

import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(exported = false)
public interface FilesRepository {
}
