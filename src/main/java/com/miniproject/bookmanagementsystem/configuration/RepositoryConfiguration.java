package com.miniproject.bookmanagementsystem.configuration;

import com.miniproject.bookmanagementsystem.model.Book;
import com.miniproject.bookmanagementsystem.model.Category;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class RepositoryConfiguration implements RepositoryRestConfigurer {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Category.class, Book.class);
    }
}
